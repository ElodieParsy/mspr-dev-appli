/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msprgosecuri;

import java.io.*;
import java.util.*;

/**
 *
 * @author Valentin
 */
public class MSPRGOSecuri {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String originefichier =  "..\\web\\views" ;
        
        //Création du fichier index.html dans lequel est affiché la liste des user
        File fUserList = new File(originefichier + "\\userlist\\userlist.html");
        printHtml(fUserList, "index",originefichier);
        
        //Récupération de la liste des user 
        File fUser = new File(originefichier + "\\userList\\user.txt");
        List listUser = RecupererListe(fUser);

        //Création des fichier en .html pour chaque user avec la liste des objets qui leur appartiennent
        for(int i =0 ; i<listUser.size(); i++){
            File fUserHTML = new File(originefichier + "\\user\\user.html");
            String sNomUser = listUser.get(i).toString(); 
            System.out.println(sNomUser);
            printHtml(fUserHTML, sNomUser, originefichier );
        }           
    }

    public static void printHtml(File monFichier,String sMonUser,String originefichier  ) {
        //Fonction qui Crée un html a partir d'un template ici monFichier , sMonUser sert à récupérer le txt et nommer le fichier
        try {
            FileReader frMonFichier = new FileReader(monFichier);
            BufferedReader brMonFichier = new BufferedReader(frMonFichier);
            StringBuilder sbuserList = new StringBuilder();
            String line;
            while ((line = brMonFichier.readLine()) != null) {
                switch(line) {
                    case "    {{ item.list }}" :
                        // Cas ou on remplit le html de la liste des materiels dans la page d'un user
                        File fMateriel = new File(originefichier + "\\userList\\ListeMateriel.txt");
                        List listMateriel = RecupererListe(fMateriel);
                        
                        File fListeMaterielObtenus = new File(originefichier + "\\userList\\"+sMonUser+".txt");
                        List listMaterielObtenus = RecupererListe(fListeMaterielObtenus);
                        
                        String sChecked = "";
                        for(int i =0 ; i<listMateriel.size(); i++){
                            for(int j =0 ; j<listMaterielObtenus.size(); j++){
                                if (listMaterielObtenus.get(j).toString().equalsIgnoreCase(listMateriel.get(i).toString())){
                                    sChecked = "checked";
                                    break;
                                }else{
                                    sChecked = "";
                                }
                            }
                            sbuserList.append("<li class='item'>\n");
                            sbuserList.append("<label for='item"+ i+"'>" + listMateriel.get(i) + "</label>\n");
                            sbuserList.append("<input type='checkbox' name='item"+i +"' "+sChecked+">\n");
                            sbuserList.append("</li>\n");
                            sbuserList.append("\n");
                        }
                        break;
                    case "{{ id-card-img }}": 
                         sbuserList.append("<img id=\"idcard\" src=\"../assets/img/" + sMonUser + "\" />\n");
                        break;
                    case "      {{ user.name }}" :
                        // Cas ou on remplit le html de l'user dans la page d'un user 
                        sbuserList.append("<li class='item'>\n");
                        sbuserList.append("<label for='item'>" + sMonUser + "</label>\n");
                        sbuserList.append("</li>\n");
                        sbuserList.append("\n");
                        break;
                    case "        {{ user.link }":
                        // Cas ou on remplit le html de la liste des utlisateur dans l'index
                        File fUser = new File(originefichier + "\\userList\\user.txt");
                        List listUser = RecupererListe(fUser);
                        for(int i =0 ; i<listUser.size(); i++){
                            sbuserList.append("<li class='user'>\n");
                            sbuserList.append("<a href=\"" + listUser.get(i)+ ".html"+ "\">"+ listUser.get(i) + "</a> \n");
                            sbuserList.append("</li>\n");
                            sbuserList.append("\n");
                        }
                        break;      
                    default  :
                        //Remplit le html avec les lignes qui ne doivent pas bouger du template
                        sbuserList.append(line);
                        break;    
                    }
                sbuserList.append("\n");
            }
            //Supprime l'ancien fichier et en crée un nouveau à la place
            frMonFichier.close();
            File fMonFichier = new File(sMonUser+".html");
            fMonFichier.delete();
            FileWriter writer = new FileWriter(fMonFichier, true);
            writer.write(sbuserList.toString());
            writer.close();
        } catch (IOException e) {
        }
    }

    public static List RecupererListe(File fMaListe) {
        //Fonction qui transforme un fichier qu'on lui passe en parametre et qui renseigne une liste 
        ArrayList myArrayList = new ArrayList();
        int parcoursListe = 0;
        try {
            FileReader frObjet = new FileReader(fMaListe);
            BufferedReader brObjet = new BufferedReader(frObjet);
            String line;
            while ((line = brObjet.readLine()) != null) {
                myArrayList.add(parcoursListe, line);
                parcoursListe++;
            }
            frObjet.close();
        } 
        catch (IOException e) {
        }
        return myArrayList;
    }
}
