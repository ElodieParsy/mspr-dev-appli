const credentials = [
  {
    username: "admin",
    password: "admin",
  }
];

function areCredentialsValid(username, password) {
  for (let i = 0; i < credentials.length; i++) {
    if (
      username == credentials[i].username &&
      password == credentials[i].password
    ) {
      return true;
    }
  }
  return false;
}

function login() {
  const username = document.getElementById("email").value;
  const password = document.getElementById("password").value;

  if (areCredentialsValid(username, password)) {
    window.location.replace("../userlist/userlist.html");
  }
  else {

    const credInvalidBox = document.getElementById("credentials-invalid");
    credInvalidBox.style.padding = "5px";
    credInvalidBox.style.height = "45px";
    credInvalidBox.style.border = "2px solid #ff0000c5";
  }
}